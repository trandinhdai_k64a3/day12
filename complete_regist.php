<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/style.css">
    <title>Đăng kí thành công</title>
</head>
<body>
    <div class="center">
        <div class="container" style="display: flex; align-items: center; flex-direction: column;">
        <div>Bạn đã đăng kí thành công sinh viên</div>
        <div>
            <a href="list_tsv.php" style="color:black;">Quay lại danh sách sinh viên</a>
        </div>

        </div>
    </div>
</body>
</html>