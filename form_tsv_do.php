<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="./css/style.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
</head>
<style><?php include './css/style.css' ?></style>
<body>

    <?php
        $name_student = $admission_date = $gender = $department = $student_address = $student_image = ' ';
        // if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            
            $db = new mysqli('localhost', 'root', '', 'student_management');
            if ($db->connect_error) {
                die('Connect Error (' . $db->connect_errno . ') ' . $db->connect_error);
            }
            $db->set_charset('utf8');
            // insert data
            $name_student = $_SESSION['data']['name_student'];
            $admission_date = $_SESSION['data']['admission_date'];
            $gender = $_SESSION['data']['gender'];
            $department = $_SESSION['data']['department'];
            $student_address = $_SESSION['data']['student_address'];
            if(isset($_SESSION['data']['student_image'])) {
                $student_image = $_SESSION['data']['student_image'];
            }else {
                $student_image = ' ';
            }
            if (isset($student_image)){
                if (isset($student_address)){
                    $sql = "INSERT INTO student (`name`, `gender`, `faculty`, `birthday`, `address`, `avatar`) VALUES ('".$name_student."', ".$gender.", '".$department."', '".$admission_date."', '".$student_address."', '".$student_image."');";
                }else{
                    $sql = "INSERT INTO student (`name`, `gender`, `faculty`, `birthday`, `avatar`) VALUES ('".$name_student."', ".$gender.", '".$department."', '".$admission_date."', '".$student_image."');";
                }               
            }else{
                if (isset($student_address)){
                    $sql = "INSERT INTO student (`name`, `gender`, `faculty`, `birthday`, `address`) VALUES ('".$name_student."', ".$gender.", '".$department."', '".$admission_date."', '".$student_address."');";
                }else{
                    $sql = "INSERT INTO student (`name`, `gender`, `faculty`, `birthday`) VALUES ('".$name_student."', ".$gender.", '".$department."', '".$admission_date."');";
                }               
            }
            
            echo $sql;
            $db -> query($sql );
        
    ?>

    <form action = 'complete_regist.php' method="post">
    <div class="center">
        <div class="container">
                <div class="border_box">
                    <span class="label red">Họ và tên</span>
                    <div class="result">
                        <?php
                            echo $_SESSION['data']['name_student'];
                        ?>
                    </div>
                </div>
                <div class="border_box">
                    <span class="label red">Giới tính</span>
                    <div class="result">
                        <?php
							if ($_SESSION['data']['gender'] == 1) {
                                echo $_SESSION["listGender"][1];
                            } else {
                                echo $_SESSION["listGender"][2];
                            }
                        ?>
                    </div>
                </div>
                <div class="border_box">
                    <span class="label red">Phân khoa</span>
                    <div class="result">
                        <p>
                            <?php
                                $department = $_SESSION['data']['department'];
                                if ($department == "MAT") {
                                    echo '<option value="MAT" selected>Khoa học máy tính</option>';
                                } else if ($department == "KDL") {
                                    echo '<option value="KDL" selected>Khoa học vật liệu</option>';
                                } else {
                                    echo '<option value="None" selected>None</option>';
                                }
                            ?>
                        </p>
                    </div>
                </div>

                <div class="border_box">
                    <span class="label red">Ngày sinh</span>
                    <div class="result">
                        <?php
                            echo $_SESSION['data']['admission_date'];
                        ?>
                    </div>
                </div>
                <div class="border_box">
                    <span class="label">Địa chỉ</span>
                    <div class = 'result'>
                        <?php
                            echo $_SESSION['data']['student_address'];
                        ?>
                    </div>
                </div>
                <div class="border_box">
                    <span class="label">Hình ảnh</span>
                    <div class = 'result'>
                    
                        <img src="<?php echo $_SESSION['data']["student_image"] ?>" alt="" width="100" height="150%">
                        

                    </div>
                </div>
                <div class="login-btn">
                        <button class="button_submit" type="submit" name="submit">Xác nhận</button>
                </div>    
        </div>
    </div>
    </form>
</body>
</html>